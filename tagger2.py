#Jakob Coles
#January 22, 2018
#Tags documents based on TFIDF score of keywords. No stemming is done.
#See README for data sources.
from os import listdir
from math import log
from re import sub
TAGLEN = 3 #how many tags to display
cdir = listdir("corpus") #a pretty bad corpus, will make a better one someday
dicts = []
#Makes a list of lists of words in each documents, filtering out stopwords.
docs = [list(filter(lambda x:x not in open("stopwords.txt",encoding="utf-8").read().split(","), sub(r',|\.|\(|\)|"|\[|\]','',open("corpus/"+txt,encoding = "utf-8").read().lower()).split())) for txt in cdir]
#For each document, make a dictionary mapping each word to how many times it appears in the document.
for text in docs:
    dct = {}
    for word in text:
        dct[word] = dct.get(word, 0) + 1 #pretty fast, get() averages O(1) thanks to underlying hashmap structure
    dicts += [dct]
#Calculates TFIDF score for each word, strips out duplicates, then sorts the rest by score and extracts the top TAGLEN elements.
scores = list(map(lambda y:y[:TAGLEN], [sorted(text, key=lambda x:x[1], reverse=True) for text in [list({tup: 1 for tup in score}.keys()) for score in [[(word, float(dicts[i][word])/float(len(dicts[i]))*(1.0 + log(float(len(dicts))/float(sum([dct.get(word, 0) > 0 for dct in dicts]))))) for word in docs[i]] for i in range(len(dicts))]]]))
#Display
for i in range(len(scores)):
    print("Keywords for ", cdir[i], ":", str(scores[i])[1:-1])
