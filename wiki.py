from requests import get
from os import listdir
from math import log
REQS = 100
MAX_TAGS = 3
corpus = [open("corpus/"+file,encoding="utf-8").read().split() for file in listdir("corpus")]
stopwords = open("wikistopwords.txt").read().split(",")
for i in range(REQS):
    #do stuff
    #shamelessly ripped from https://stackoverflow.com/questions/4452102/how-to-get-plain-text-out-of-wikipedia
    art = list(filter(lambda x:x not in stopwords, next(iter(get('https://en.wikipedia.org/w/api.php',params={'action':'query','generator':'random','prop':'extracts','grnnamespace':'0','format':'json','explaintext':True}).json()['query']['pages'].values()))['extract'].replace("=","").split()))
    
    dct = {}
    for word in art:
        dct[word] = dct.get(word, 0) + 1
    ldct = list(dct)
    tfidfs = list(sorted([(word, (float(dct[word])/float(len(ldct)))*(log(float(len(corpus))/float(1.0+len(list(filter(lambda x:word in x, corpus))))))) for word in ldct], key=lambda x:x[1],reverse=True))[:MAX_TAGS]
    print(tfidfs)    
    
print()

